import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from numpy.linalg import inv


def non_func(x):
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y


def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy


x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)


np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

x = x[:, np.newaxis]
x = (np.column_stack((np.ones(np.size(x)), x)))

y_measured = y_measured[:, np.newaxis]

xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]

xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)

print('Model je oblika y_hat = Theta0 + Theta1 * x')
print('y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x')

ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)

a = np.matmul(inv(np.matmul(np.transpose(xtrain), xtrain)), np.transpose(xtrain))
b = np.matmul(thetattt00, ytrain)
print(b)

theta_old = [[0.5], [0.5]]
alfa = 0.04

plt.figure()

for i in range(10):
    h_theta = np.matmul(xtrain, theta_old)

    nesto = h_theta - ytrain
    nesto = np.transpose(nesto)

    d0 = (1/70) * np.sum(np.matmul(nesto, np.transpose(xtrain[:, 0])))
    d1 = (1/70) * np.sum(np.matmul(nesto, np.transpose(xtrain[:, 1])))

    thetha0 = theta_old[0] - alfa*d0
    thetha1 = theta_old[1] - alfa*d1

    theta_old[0] = thetha_new0
    theta_old[1] = thetha_new1

    plt.plot([0, 10], [theta_old[0], theta_old[1]*10+theta_old[0]])
    plt.pause(0.05)

    print(str(i) + ": " + str(theta_old))
