import re

name=input("Unesi ime datoteke:")

file=open(name)

lista= []

for line in file:
    if line.startswith("From"):
        app=re.search(r'[\w\.-]+@[\w\.-]+', line)
        lista.append(app.group(0))

rijecnik = dict()
for element in lista:
    domena= element.split('@')[1]
    if domena not in rijecnik:
        rijecnik[domena]=1
    else:
        rijecnik[domena] +=1
print(rijecnik)
