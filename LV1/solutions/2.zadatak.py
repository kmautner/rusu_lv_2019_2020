import sys


try:
    ocjena=float(input("Unesi ocjenu"))
except ValueError:
    print("nije broj")
    sys.exit()


if float(ocjena)>=0.9 and float(ocjena)<1.1:
    print("A")
elif float(ocjena)>=0.8 and float(ocjena)<1.1:
    print("B")
elif float(ocjena)>=0.7 and float(ocjena)<1.1:
    print("C")
elif float(ocjena)>=0.6 and float(ocjena)<1.1:
    print("D")
elif float(ocjena)>=0.0 and float(ocjena)<1.1:
    print("F")
else:
    print("Izvan opsega")