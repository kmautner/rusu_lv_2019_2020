import re

name = input ("Unesi ime datoteke: ")
file = open(name)
lista = []
for line in file:
    if line.startswith("X-DSPAM-Confidence: "):
        x = re.findall("\d+\.\d+", line)
        lista.append(float(x[0]))

print("Srednja vrijednost: ", sum(lista)/len(lista))
