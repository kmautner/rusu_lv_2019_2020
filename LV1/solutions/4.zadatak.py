import sys
petlja = 1
brojac = 0
zbroj=0
maks=input("unesi broj ili done za kraj")
if(maks=='done'):
    petlja=0
    maks=0
    minim=0
try:
    val=int(maks)
    minim=maks
except ValueError:
    print("nije broj")
    sys.exit()
while(petlja==1):
    broj=input("unesi broj ili done za kraj")
    if(broj =='done'):
        petlja=0
        break
    try:
        val=int(broj)
        if(int(broj)>int(maks)):
            maks=broj
        elif(int(broj)<int(minim)):
            minim=broj
        zbroj+=int(broj)
    except ValueError:
        print("nije broj")
        sys.exit()
    
print("maks je " + str(maks))
print("min je " + str(minim))
print("zbroj je " + str(zbroj))
