import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')

zad1 = mtcars.groupby('cyl').mean()
plt.bar([4, 6, 8], zad1['mpg'], align='center', alpha= 0.5)
plt.show()

zad2 = mtcars[['cyl', 'wt']]
zad2.boxplot(by='cyl')
plt.show()

zad3 = mtcars.groupby('am').mean()
print(zad3)
plt.bar([0, 1], zad3['mpg'], align='center', alpha=1, linewidth = 0.1)
plt.show()

plt.scatter(mtcars[mtcars.am == 0]['qsec'], mtcars[mtcars.am == 0]['hp'], label='automatic', s=mtcars[mtcars.am == 0]['wt']*20)
plt.scatter(mtcars[mtcars.am == 1]['qsec'], mtcars[mtcars.am == 1]['hp'], label='manual', s=mtcars[mtcars.am == 1]['wt']*20)
plt.show()
