from sklearn import datasets, cluster
import numpy as np
import matplotlib.pyplot as plt


def generate_data(n_samples, flagc):
    if flagc == 1:
        random_state = 365
        X, y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)

    elif flagc == 2:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)

    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                                   cluster_std=[1.0, 2.5, 0.5, 3.0],
                                   random_state=random_state, centers=4)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)

    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)

    else:
        X = []

    return X
X = generate_data(500, 5)
k_means = cluster.KMeans(n_clusters=5, n_init=1)
k_means.fit(X)
labels = k_means.predict(X)

x = X[:, 0]
y = X[:, 1]

for i in range(5):
    xL = []
    yL = []
    for k in range(500):
        if labels[k] == i:
            xL.append(x[k])
            yL.append(y[k])

    plt.scatter(xL, yL, c=np.random.rand(3, ))


plt.scatter(k_means.cluster_centers_[:, 0], k_means.cluster_centers_[:, 1], c='green')
plt.show()
