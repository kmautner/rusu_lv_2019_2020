import numpy as np
import matplotlib.pyplot as plt

np.random.seed(56)
randomNumbers = np.random.randint(2, size=10)

m = 100
n = 2
v = np.random.normal(m,n,1)

visine_m = []

visine_z = []
for i in randomNumbers:
    if i == 0:
        visine_z.append(np.random.normal(167, 7))
    else:
        visine_m.append(np.random.normal(180, 7))

plt.hist([visine_m, visine_z], color = ['blue', 'red'])


avg_m = np.average(visine_m)
avg_z = np.average(visine_z)

plt.axvline(avg_m, color='green')
plt.axvline(avg_z, color='pink')
plt.legend(["m", "z", "avg_m", "avg_z"])
